data:extend({
  {
    type = "bool-setting",
    name = "smr-autodeconstruct",
    order = "a",
    setting_type = "startup",
    default_value = false,
  },
  {
    type = "double-setting",
    name = "smr-1-conversion-factor",
    order = "a",
    setting_type = "startup",
    default_value = 0.625,
    minimum_value = 0.25,
    maximum_value = 1
  },
  {
    type = "double-setting",
    name = "smr-2-conversion-factor-create",
    order = "a",
    setting_type = "startup",
    default_value = 0.78,
    minimum_value = 0.25,
    maximum_value = 1
  },
  {
    type = "double-setting",
    name = "smr-2-conversion-factor-refill",
    order = "a",
    setting_type = "startup",
    default_value = 0.83,
    minimum_value = 0.25,
    maximum_value = 1
  },
  {
    type = "double-setting",
    name = "smr-3-conversion-factor-create",
    order = "a",
    setting_type = "startup",
    default_value = 0.89,
    minimum_value = 0.25,
    maximum_value = 1
  },
  {
    type = "double-setting",
    name = "smr-3-conversion-factor-refill",
    order = "a",
    setting_type = "startup",
    default_value = 0.96,
    minimum_value = 0.25,
    maximum_value = 1
  },
  {
    type = "int-setting",
    name = "smr-1-capacity",
    order = "a",
    setting_type = "startup",
    default_value = 10,
    minimum_value = 5,
    maximum_value = 50
  },
  {
    type = "int-setting",
    name = "smr-1-output",
    order = "a",
    setting_type = "startup",
    default_value = 10,
    minimum_value = 5,
    maximum_value = 50
  },
  {
    type = "int-setting",
    name = "smr-2-capacity",
    order = "a",
    setting_type = "startup",
    default_value = 100,
    minimum_value = 50,
    maximum_value = 500
  },
  {
    type = "int-setting",
    name = "smr-2-output",
    order = "a",
    setting_type = "startup",
    default_value = 25,
    minimum_value = 10,
    maximum_value = 100
  },
  {
    type = "int-setting",
    name = "smr-3-capacity",
    order = "a",
    setting_type = "startup",
    default_value = 500,
    minimum_value = 250,
    maximum_value = 1000
  },
  {
    type = "int-setting",
    name = "smr-3-output",
    order = "a",
    setting_type = "startup",
    default_value = 50,
    minimum_value = 25,
    maximum_value = 250
  }
})
